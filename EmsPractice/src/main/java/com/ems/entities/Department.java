package com.ems.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "department")
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "department_id")
	private int id;
	@Column(name = "department_name", unique = true)
	private String departmentName;
	@JsonManagedReference
	@OneToMany(mappedBy = "department", fetch = FetchType.LAZY)
	private List<User> employeeList;

	public Department() {
		// TODO Auto-generated constructor stub
	}
	
	public Department(int id, String departmentName) {
		super();
		this.id = id;
		this.departmentName = departmentName;
		this.employeeList = new ArrayList<User>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	//@JsonManagedReference
	public List<User> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<User> employeeList) {
		this.employeeList = employeeList;
	}

	public  void addEmployee(User user) {
		employeeList.add(user);
		user.setDepartment(this);
	}
	
	@Override
	public String toString() {
		return "Department [id=" + id + ", departmentName=" + departmentName + "]";
	}

	

}
