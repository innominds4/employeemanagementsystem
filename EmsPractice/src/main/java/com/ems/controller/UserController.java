package com.ems.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ems.dtos.Response;
import com.ems.entities.Address;
import com.ems.entities.JobHistory;
import com.ems.services.AddressService;
import com.ems.services.DepartmentService;
import com.ems.services.JobHistoryService;
import com.ems.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService service;
	
	@Autowired
	private DepartmentService deptService;
	
	@Autowired
	private AddressService addService;
	
	@Autowired
	private JobHistoryService jobService;
	
	
	@PostMapping("/addAddress")
	public ResponseEntity<?> addAddress(@RequestBody Address address){
		Map<String , Object> result = addService.addAddress(address);
		return Response.success(result);
	}
	
	@GetMapping("/address/{id}")
	public ResponseEntity<?> getAddressByUserId(@PathVariable("id") int id){
		Map<String,Object> result = addService.findByUserID(id);
		return Response.success(result);
	}
	
	@PutMapping("/address")
	public ResponseEntity<?> updateAddress(@RequestBody Address add ){
		Map<String, Object> address = addService.updateAddress(add);
		return Response.success(address);
	}
	
	@DeleteMapping("/address/{id}")
	public ResponseEntity<?> deleteByAddressId(@PathVariable("id") int id){
		Map<String, Object> result = addService.removeAddress(id);
		
		return Response.success(result);
	}
	
	@PostMapping("/job")
	public ResponseEntity<?> addJob(@RequestBody JobHistory job){
		Map<String, Object > result = jobService.addJob(job);
		return Response.success(result);
	}
	
	@GetMapping("/job/{id}")
	public ResponseEntity<?> findByUserId(@PathVariable("id") int id){
		JobHistory jh = jobService.findByUserId(id);
		if (jh!=null) {
			return Response.success(jh);
		}
		return Response.error("Some error");
	}
	
}
