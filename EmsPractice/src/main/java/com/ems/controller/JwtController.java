package com.ems.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ems.dtos.Credentials;
import com.ems.dtos.Response;
import com.ems.entities.User;
import com.ems.services.UserService;
import com.ems.utils.JwtUtil;

@RestController
public class JwtController {

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private UserService userService;
	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping("/token")
	public ResponseEntity<?> generateToken(@RequestBody Credentials cred) throws Exception {
		System.out.println("here 1");
		System.out.println(cred.getEmail() + "======" + cred.getPassword());
		User userDetails = userService.findByEmailAndPassword(cred.getEmail(), cred.getPassword());
		System.out.println(userDetails);
		Map<String, Object> map = new HashMap<>(); 
		System.out.println("here 2");
		if (userDetails == null)
			return Response.error("Invalid username or password");
		//map.put("userDetails", userDetails);
		try {
			authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(cred.getEmail(), cred.getPassword()));

		} catch (AuthenticationException e) {
			throw new Exception("Invalid Email/password");
		}
		/*
		 * if(userDetails.getRole().equalsIgnoreCase("admin")) map.put("admin",
		 * userDetails); else map.put("user", userDetails);
		 */
		String token = null;
		if(userDetails.getRole().equalsIgnoreCase("admin"))
			token = jwtUtil.generateToken(cred.getEmail(), userDetails.getRole(), true);
		else
			token = jwtUtil.generateToken(cred.getEmail(), userDetails.getRole(), false);
		System.out.println(token);
		
		map.put("token", token);
		return Response.success(map);
	}

}
