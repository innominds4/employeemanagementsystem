package com.ems.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ems.dtos.Response;
import com.ems.entities.Address;
import com.ems.entities.Department;
import com.ems.entities.JobHistory;
import com.ems.entities.User;
import com.ems.services.AddressService;
import com.ems.services.DepartmentService;
import com.ems.services.JobHistoryService;
import com.ems.services.UserService;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;;
	@Autowired
	private AddressService addressService;
	@Autowired
	private JobHistoryService jobService;
	
	
	
	@PostMapping("/add")
	public ResponseEntity<?> addEmployee(@RequestBody User emp) {
		User user = userService.saveUser(emp);
		if (user != null)
			return Response.success(user);
		return Response.error("Failed to add Employee");
	}

	@GetMapping("/getAll")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllEmployee() {
		List<User> employees = userService.findAllUsers();
		if (employees != null)
			return Response.success(employees);
		return Response.error("No Employees Available");
	}

	@GetMapping("/getEmployee/{id}")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getEmployee(@PathVariable("id") int id) {
		User requiredUser = userService.findByUserId(id);
		if (requiredUser != null)
			return Response.success(requiredUser);
		return Response.error("No Employee With this id");

	}

	@DeleteMapping("deleteEmployeeById/{id}")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
		Map<String, Object> result = userService.removeUser(id);
		return Response.success(result);
	}

	
	@GetMapping("/department/{id}")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getDepartment(@PathVariable("id") int departmentId){
		Department department = departmentService.getDepartmentById(departmentId);
		if(department!=null)
			return Response.success(department);
		return Response.error(department);
	}
	
	@GetMapping("/allDepartments")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllDepartments(){
		List<Department> departments = departmentService.getAllDepartments();
		if(departments.isEmpty())
			return Response.error("No record of  department");
		return Response.success(departments);
	}
	
	@PostMapping("/addDepartment")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> addDepartment(@RequestBody Department department) {

		Map<String, Object> newDepartment = departmentService.addDepartment(department);
		if (newDepartment != null)
			return Response.success(newDepartment);
		else
			return Response.error("Failed to add Department");

	}
	
	
	@GetMapping("/allAddress")
	public ResponseEntity<?> getAllAddress(){
		List<Address> add = addressService.findAllAdd();
		return Response.success(add);
	}
	
	@GetMapping("/allJob")
	public ResponseEntity<?> getAllJobs(){
		List<JobHistory> jobs = jobService.findAllJobs();
		return Response.success(jobs);
	}
	
	@GetMapping("/address/{id}")
	public ResponseEntity<?> getAddressByUserId(@PathVariable("id") int id){
		Map<String,Object> result = addressService.findByUserID(id);
		return Response.success(result);
	}
	
	@GetMapping("/job/{id}")
	public ResponseEntity<?> findByUserId(@PathVariable("id") int id){
		JobHistory jh = jobService.findByUserId(id);
		return Response.success(jh);
	}
}
