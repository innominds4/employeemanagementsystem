package com.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication//(exclude = SecurityAutoConfiguration.class)
public class EmsPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsPracticeApplication.class, args);
		System.out.println("Spring boot started");
	}

}
