package com.ems.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ems.services.UserService;
import com.ems.utils.JwtUtil;

@Component
public class JwtFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private UserService userService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// get jwt
		// check starting from bearer or not
		// validate
		String mapping = request.getRequestURI();
		String authorizationHeader = request.getHeader("Authorization");
		System.out.println(authorizationHeader);
		String role = null;
		boolean isAdmin = false;
		boolean roleAuthorized = false;
		String token = null;
		String email = null;
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			System.out.println("in if block");
			token = authorizationHeader.substring("Bearer ".length());
			email = jwtUtil.extractUsername(token);
			role = jwtUtil.extractClaim(token, (e) -> e.get("role", String.class));
			isAdmin = jwtUtil.extractClaim(token, (e) -> e.get("isAdmin", Boolean.class));
		}
		if (token != null) {
			switch (role.toUpperCase()) {
			case "USER":
				if (mapping.startsWith("/user"))
					roleAuthorized = true;
				break;
			case "ADMIN":
				if (mapping.startsWith("/admin") && isAdmin)
					roleAuthorized = true;
				break;
			default:
				break;

			}
		}
		System.out.println(token);
		if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = userService.loadUserByUsername(email);
			if (jwtUtil.validateToken(token, userDetails) && roleAuthorized) {
				System.out.println("validated");
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

			}
		}
		filterChain.doFilter(request, response);

	}

}
