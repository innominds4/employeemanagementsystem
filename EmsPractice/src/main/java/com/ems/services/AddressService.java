package com.ems.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ems.daos.AddressDao;
import com.ems.entities.Address;
import com.ems.entities.User;

@Service
@Transactional
public class AddressService {

	@Autowired
	private AddressDao addressDao;

	public Address findByAddId(int id) {
		return addressDao.findById(id);
	}

	public List<Address> findAllAdd() {
		return addressDao.findAll();
	}

	public Map<String, Object> addAddress(Address address) {
		Address add = addressDao.save(address);
		return Collections.singletonMap("Address saved", add.getId());
	}

	public Map<String, Object> removeAddress(int id) {
		if (addressDao.existsById(id)) {

			addressDao.deleteById(id);
			return Collections.singletonMap("Address deleted", 1);
		}
		return Collections.singletonMap("Address not found", 0);
	}

	public Map<String, Object> updateAddress(Address add) {
		if (addressDao.existsById(add.getId())) {

			Address address = addressDao.save(add);
			return Collections.singletonMap("Address updated...!!!", 1);
		}
		return null;

	}

	public Map<String, Object> findByUserID(int id) {
		List<Address> add = addressDao.findByUserId(id);
		return Collections.singletonMap("Address ", add);
	}

	public List<User> findUserByAddressId(String pincode) {
		return addressDao.findUserByPincode(pincode);
	}

}
