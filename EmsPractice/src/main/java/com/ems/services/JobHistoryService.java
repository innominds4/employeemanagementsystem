package com.ems.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ems.daos.JobHistoryDao;
import com.ems.entities.JobHistory;

@Service
@Transactional
public class JobHistoryService {

	@Autowired
	private JobHistoryDao jobDao;
	
	public JobHistory findByJobId(int id) {
		JobHistory job = jobDao.findById(id);
		return job;
	}
	
	public JobHistory findByUserId(int user_id) {
		JobHistory job = jobDao.findByUserId(user_id);
		return job;
	}
	
	public Map<String, Object> addJob(JobHistory job){
		JobHistory jh = jobDao.save(job);
		return Collections.singletonMap("JobHistoty saved",jh.getId());
	}
	
	public List<JobHistory> findAllJobs(){
		return jobDao.findAll();
	}
	

}
