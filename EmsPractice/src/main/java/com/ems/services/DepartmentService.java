package com.ems.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ems.daos.DepartmentDao;
import com.ems.entities.Department;
import com.ems.entities.User;

@Transactional
@Service
public class DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;

	public Department getDepartmentById(int departmentId) {
		Department department = departmentDao.findDepartmentById(departmentId);
		if (department != null)
			return department;
		return null;
	}

	public Map<String, Object> addDepartment(Department departmentToBeAdded) {
		if (departmentDao.findByDepartmentName(departmentToBeAdded.getDepartmentName()) != null)
			return null;
		Department newDepartment = departmentDao.save(departmentToBeAdded);
		return Collections.singletonMap("departmentAdded", newDepartment.getId());
	}

	public Map<String, Object> removeDepartment(int id) {
		if (departmentDao.existsById(id)) {
			departmentDao.deleteById(id);
			return Collections.singletonMap("affected rows", 1);
		}
		return Collections.singletonMap("affected rows", 0);

	}

	public List<Department> getAllDepartments() {
		List<Department> departments = new ArrayList<Department>();
		departments = departmentDao.findAll();
		return departments;
	}

	public Map<String, Object> renameDepartment(int id, String name) {
		Department department = departmentDao.findDepartmentById(id);
		if (department != null) {
			department.setDepartmentName(name);
			department = departmentDao.save(department);
			return Collections.singletonMap("department renamed", 1);
		}
		return Collections.singletonMap("department renamed", 0);
	}

	public List<User> findUserByDept(int id) {
		List<User> userList = new ArrayList<User>();
		userList = departmentDao.findByDepartmentId(id);
		return userList;
	}

}
