package com.ems.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ems.entities.JobHistory;

@Repository
public interface JobHistoryDao extends JpaRepository<JobHistory, Integer>{

	JobHistory findById(int id);
	
	@Query(value = "SELECT * FROM job_history WHERE user_id = ?;", nativeQuery = true)
	JobHistory findByUserId(int userId);
	
}
