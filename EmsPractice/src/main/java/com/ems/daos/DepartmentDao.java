package com.ems.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ems.entities.Department;
import com.ems.entities.User;

public interface DepartmentDao extends JpaRepository<Department, Integer>{

	Department findDepartmentById(int departmentId);
	
	Department findByDepartmentName(String departmentName);
	Department deleteById(int id);
	
	@Query(value = "SELECT * FROM user_emp where department_id = ?;", nativeQuery = true)
	List<User> findByDepartmentId(int id);
}
