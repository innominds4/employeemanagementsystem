package com.ems.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ems.entities.Address;
import com.ems.entities.User;

@Repository
public interface AddressDao extends JpaRepository<Address, Integer>{

	Address findById(int id);
	
	@Query(value = "SELECT * from address where user_id = ?;", nativeQuery = true)
	List<Address> findByUserId(int user_id);
	
	@Query(value =  "SELECT * FROM user_emp WHERE user_id IN(select user_id from address where id = ? );", nativeQuery = true)
	List<User> findUserByPincode(String pincode);
}
