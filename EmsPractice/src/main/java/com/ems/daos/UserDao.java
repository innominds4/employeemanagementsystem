package com.ems.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ems.entities.User;

public interface UserDao extends JpaRepository<User, Integer>{
	
	User findUserById(int userId);
	
	User findByName(String name);
	
	User findByRole(String role);
	
	User findByEmail(String email);
	
	
	

}
