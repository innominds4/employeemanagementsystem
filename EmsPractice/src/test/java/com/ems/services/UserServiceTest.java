package com.ems.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ems.daos.UserDao;
import com.ems.entities.User;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceTest {

	@Mock
	private UserDao dao;

	@InjectMocks
	private UserService service;

	@Mock
	private PasswordEncoder passwordEncoder;

//	private List<User> userList;
//	@Autowired
//	private DepartmentDao deptDao;

//	@Transactional
	@Test
	public void getUsersTest() {
//		Department findDepartmentById = deptDao.findDepartmentById(2);
		when(dao.findAll()).thenReturn(Stream
				.of(new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null),
						new User(20, "saket", "sa@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null))
				.collect(Collectors.toList()));
		assertEquals(2, service.findAllUsers().size());
	}

	@Test
	public void getUserTest() {
		when(dao.findUserById(14)).thenReturn(
				(new User(14, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null)));
		assertEquals("sanket", service.findByUserId(14).getName());
		when(dao.findUserById(2)).thenReturn(null);
		assertEquals(null, service.findByUserId(2));
	}

	@Test
	public void saveUserTest() {
		User user = new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		when(dao.save(user)).thenReturn(user);
		when(passwordEncoder.encode(user.getPassword()))
				.thenReturn("$2a$12$p7MAN5vw7xjbZo8hs8uSNu.rkoQwb4SCM86lZlZ2axFSb575QnEq2");
		assertEquals(user, service.saveUser(user));
		
		when(dao.findByEmail("ak@gmail.com")).thenReturn(user);
		when(service.findByEmail("ak@gmail.com")).thenReturn(user);
		assertEquals(null, service.saveUser(user));
	}

	@Test
	public void updateUserTest() {
		User user = new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		User newuser = new User(20, "Adesh", "ak@gmail.com", "1234", "00000000", "Male", "User", 100000.00, null);
		when(service.findByEmail(user.getEmail())).thenReturn(user);
		when(dao.save(user)).thenReturn(user);
		System.out.println(Collections.singletonMap("updatedUser", newuser).get(newuser));
		assertEquals(Collections.singletonMap("updatedUser", newuser).get(newuser), service.update(newuser).get(newuser));
	
		when(service.findByEmail("ak@gmail.com")).thenReturn(null);
		assertEquals(Collections.singletonMap("updated rows", 0), service.update(user));
	}
	
	
	@Test
	public void removeUserTest() {
		User user = new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		when(dao.existsById(20)).thenReturn(true);
		service.removeUser(20);
		verify(dao,times(1)).deleteById(20);
		when(dao.existsById(21)).thenReturn(false);
		assertEquals(Collections.singletonMap("affected rows", 0),service.removeUser(21));
	}

	@Test
	public void getAllEmp() {
		List<User> userList = new ArrayList<>();
		userList.add(new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null));
		userList.add(new User(20, "saket", "sa@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null));
		userList.add(new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null));

		when(dao.findAll()).thenReturn(userList);
		assertEquals(3, service.findAllUsers().size());
	}

	@Test
	public void authenticateTest() {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		when(service.findByEmail("sb@gmail.com")).thenReturn(user);
		when(passwordEncoder.matches("1234","$2a$12$p7MAN5vw7xjbZo8hs8uSNu.rkoQwb4SCM86lZlZ2axFSb575QnEq2" )).thenReturn(true);
		assertEquals("sanket", service.findByEmailAndPassword("sb@gmail.com", "1234").getName());
	}

	@Test
	public void findByEmail() {
		User user = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		when(dao.findByEmail(user.getEmail())).thenReturn(user);
		assertEquals("sb@gmail.com", service.findByEmail("sb@gmail.com").getEmail());
	}

	@Test
	public void deleteUser() {

		User user = new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		service.deleteUser(user);
		verify(dao, times(1)).delete(user);
	}
	
	@Test
	public void loadUserByUsername() {
		User user = new User(20, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		when(dao.findByEmail(user.getEmail())).thenReturn(user);
		assertEquals(new org.springframework.security.core.userdetails.User("ak@gmail.com", "1234", (Collection<? extends GrantedAuthority>) new ArrayList<GrantedAuthority>()), service.loadUserByUsername("ak@gmail.com"));
			
	}
	
}
