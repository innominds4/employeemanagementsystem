package com.ems.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.ems.daos.JobHistoryDao;
import com.ems.entities.JobHistory;
import com.ems.entities.User;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class JobHistoryServiceTest {

	private static SimpleDateFormat sdf;
	
	static {
		try {
			sdf = new SimpleDateFormat("dd/MM/yyyy");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Mock
	private JobHistoryDao dao;
	
	@InjectMocks
	private JobHistoryService service;
	
	@Test
	public void findByJobId() throws ParseException {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		JobHistory job = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), user);
		when(dao.findById(1)).thenReturn(job);
		assertEquals(1, service.findByJobId(1).getId());
	}
	
	@Test
	public void findByUserId() throws ParseException {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		JobHistory job = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), null);
		
		when(dao.findByUserId(21)).thenReturn(job);
		job.setUser(user);
		System.out.println(job);
		System.out.println(job.getUser());
		assertEquals(1, service.findByUserId(21).getId());
	}
	
	@Test
	public void addJobTest() throws ParseException {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		JobHistory job = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), user);
		when(dao.save(job)).thenReturn(job);
		assertEquals(Collections.singletonMap("JobHistoty saved", job.getId()), service.addJob(job));
	}
	
	@Test
	public void findAllJobTest() throws ParseException {
		List<JobHistory> jobList = new ArrayList<JobHistory>();
		jobList.add(new JobHistory(1,sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), null));
		jobList.add(new JobHistory(2,sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), null));
		jobList.add(new JobHistory(3,sdf.parse("26/05/2022"), sdf.parse("31/08/2024"), null));
	
		when(dao.findAll()).thenReturn(jobList);
		assertEquals(3, service.findAllJobs().size());
	}
}
