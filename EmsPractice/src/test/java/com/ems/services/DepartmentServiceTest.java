package com.ems.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.ems.daos.DepartmentDao;
import com.ems.entities.Department;
import com.ems.entities.User;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DepartmentServiceTest {

	@Mock
	private DepartmentDao dao;
	
	@InjectMocks
	private DepartmentService service;
	
	
	@Test
	void getDepartmentByIdTest() {
		Department dept = new Department(1, "DevOps");
		
		when(dao.findDepartmentById(1)).thenReturn(dept);
		assertEquals(dept, service.getDepartmentById(1));
		System.out.println(dept);
		when(dao.findDepartmentById(2)).thenReturn(null);
		assertEquals(null, service.getDepartmentById(2));
	}
	
	@Test
	void addDepartment() {
		Department dept = new Department(1, "DevOps");
		Department dept2 = new Department(2, "SPDE");
		when(dao.save(dept)).thenReturn(dept);
		assertEquals(Collections.singletonMap("departmentAdded",1), service.addDepartment(dept));
		when(dao.findByDepartmentName(dept2.getDepartmentName())).thenReturn(dept2);
		assertEquals(null, service.addDepartment(dept2));
	}
	
	@Test
	void removeDept() {
//		Department dept = new Department(1, "DevOps");
		when(dao.existsById(1)).thenReturn(true);
		service.removeDepartment(1);
		verify(dao, times(1)).deleteById(1);
		
		when(dao.existsById(2)).thenReturn(false);
		assertEquals(Collections.singletonMap("affected rows", 0), service.removeDepartment(2));
	}
	
	@Test
	void getAllDept() {
		List<Department> deptList = new ArrayList<>();
		deptList.add(new Department(1, "DevOps"));
		deptList.add(new Department(2, "SPDE"));
		deptList.add(new Department(3, "BDE"));
		
		when(dao.findAll()).thenReturn(deptList);
		assertEquals(3, service.getAllDepartments().size());
	}
	
	
	@Test
	void renameDeapartment() {
		Department dept = new Department(1, "DevOps");
		when(dao.findDepartmentById(1)).thenReturn(dept);
		when(dao.save(dept)).thenReturn(dept);
		assertEquals(Collections.singletonMap("department renamed", 1), service.renameDepartment(1, "SPDE"));
		
		when(dao.findDepartmentById(2)).thenReturn(null);
		assertEquals(Collections.singletonMap("department renamed", 0), service.renameDepartment(2, "SPDE"));
		
	}
	
	@Test
	void findUserByDepartment() {
		Department dept = new Department(1, "DevOps");
		User u1 =  new User(1, "Adesh", "ak@gmail.com", "1234", "123456678", "Male", "user", 50000.00, dept);
		User u2 =  new User(2, "Sanket", "sb@gmail.com", "1234", "123456678", "Male", "user", 50000.00, dept);
		User u3 =  new User(3, "Prashant", "pb@gmail.com", "1234", "123456678", "Male", "user", 50000.00, dept);
		User u4 =  new User(4, "Bhagwati", "bc@gmail.com", "1234", "123456678", "FeMale", "user", 50000.00, dept);

		List<User> userList = new ArrayList<User>();
		userList.add(u1);
		userList.add(u2);
		userList.add(u3);
		userList.add(u4);

		dept.addEmployee(u1);
		dept.addEmployee(u2);
		dept.addEmployee(u3);
		dept.addEmployee(u4);
		
		when(dao.findByDepartmentId(1)).thenReturn(userList);
		assertEquals(4, service.findUserByDept(1).size());
	}
	
	
}
