package com.ems.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ems.daos.AddressDao;
import com.ems.daos.DepartmentDao;
import com.ems.daos.JobHistoryDao;
import com.ems.daos.UserDao;
import com.ems.dtos.Response;
import com.ems.entities.Address;
import com.ems.entities.JobHistory;
import com.ems.entities.User;
import com.ems.services.AddressService;
import com.ems.services.DepartmentService;
import com.ems.services.JobHistoryService;
import com.ems.services.UserService;
import com.ems.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(value = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;


	@MockBean
	private Response response;
	
	@MockBean
	private UserService service;

	@MockBean
	private UserDao dao;

	@MockBean
	private JwtUtil jwtUtil;

	@MockBean
	private DepartmentService deptService;

	@MockBean
	private DepartmentDao deptDao;

	@MockBean
	private AddressService addService;

	@MockBean
	private JobHistoryDao jobDao;
	
	@MockBean
	private AddressDao addDao;
	
	@MockBean
	private JobHistoryService jobService;

	private static SimpleDateFormat sdf;

	static {
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}
	
	@Test
	public void addAddressTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		ObjectMapper mapper = new ObjectMapper();
		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);

		Mockito.when(addService.addAddress(Mockito.any())).thenReturn(Collections.singletonMap("Address saved", a1.getId()));
		Mockito.when(addDao.save(a1)).thenReturn(a1);
		
		String uri = "/user/addAddress";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(a1)).characterEncoding("utf-8");
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		
		assertNotNull(result.getResponse().getContentAsString());
		String output = result.getResponse().getContentAsString();
		System.out.println(output);
	
	}
	
	@Test
	public void getAddressDetailsTest() throws Exception {
		List<Address> addressList = new ArrayList<Address>();
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
		Address a2 = new Address(2, "Trends", "Hyderabad", "India", "500032", u1);
		Address a3 = new Address(3, "Trends", "Hyderabad", "India", "500032", u1);
		addressList.add(a1);
		addressList.add(a2);
		addressList.add(a3);
		Mockito.when(addService.findByUserID(21)).thenReturn(Collections.singletonMap("Address ", addressList));
		String uri = "/user/address/21";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = result.getResponse().getContentAsString();
		System.out.println(output);

	}
	
	@Test
	public void deleteAddressTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
		
		Mockito.when(addService.removeAddress(1)).thenReturn(Collections.singletonMap("Address deleted", 1));
//		Mockito.verify(addDao,times(1)).deleteById(1);
		String uri = "/user/address/1";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		
		String output = result.getResponse().getContentAsString();
		System.out.println(output);
	}
	
	
	@Test
	public void addJobTest() throws Exception  {
		String uri = "/user/job";
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		JobHistory j1 = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("30/08/2022"), u1);
		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(jobService.addJob(Mockito.any())).thenReturn(Collections.singletonMap("JobHistoty saved",j1.getId()));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(j1)).characterEncoding("utf-8");
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
	
		assertNotNull(result.getResponse().getContentAsString());
		String output = result.getResponse().getContentAsString();
		
		System.out.println(output);
	}
	
	@Test
	public void getJobDetailsTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		JobHistory j1 = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("30/08/2022"), u1);

		Mockito.when(jobService.findByUserId(21)).thenReturn(j1);
		String uri = "/user/job/21";
		String uri2 = "/user/job/22";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		Mockito.when(jobService.findByUserId(22)).thenReturn(null);
		RequestBuilder requestBuilder2 = MockMvcRequestBuilders.get(uri2).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result2 = mockMvc.perform(requestBuilder2).andDo(print()).andExpect(status().isOk()).andReturn();
		Response.error("Not Found");
		Response.status(HttpStatus.ACCEPTED);
		
		String output = result.getResponse().getContentAsString();
		String output2 = result2.getResponse().getContentAsString();
		System.out.println(output);
		System.out.println(output2);
		
	}
	
	@Test
	public void updateAddressTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
		Address a2 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
	
		ObjectMapper mapper = new ObjectMapper();
		String uri = "/user/address";
		Mockito.when(addService.updateAddress(a2)).thenReturn(Collections.singletonMap("Address updated...!!!", 1));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(a2)).characterEncoding("utf-8");
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
	}
	


}
