package com.ems.controller;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ems.daos.UserDao;
import com.ems.entities.User;
import com.ems.services.UserService;
import com.ems.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(value = JwtController.class)
@AutoConfigureMockMvc(addFilters = false)
public class JwtControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService service;

	@MockBean
	private PasswordEncoder passwordEncoder;
	
	@MockBean
	private AuthenticationManager authenticationManager;
	
	@MockBean
	private UserDao dao;

	@MockBean
	private JwtUtil jwtUtil;

	@Test
	public void generateTokenTest() throws Exception {

		User u1 = new User(21, "sanket", "sb@gmail.com", "1234",
				"1234567890", "Male", "User", 50.00, null);

		when(service.findByEmailAndPassword("sb@gmail.com", "1234")).thenReturn(u1);
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken("sb@gmail.com","1234");
		String uri = "/token";
		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class))).thenReturn(authenticationToken);
		Map<String , Object> claims = new HashMap<String, Object>();
		claims.put("role", "User");
		claims.put("isAdmin", false);
		
		Mockito.when(jwtUtil.generateToken("sb@gmail.com", "User", false)).thenReturn("12345678900987654321");
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(u1)).characterEncoding("utf-8");
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(result.getResponse().getContentAsString());
		System.out.println(result.getResponse().getContentAsString());
	}
	


}
