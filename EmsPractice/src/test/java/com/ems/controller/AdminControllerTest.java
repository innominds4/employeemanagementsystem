package com.ems.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ems.daos.DepartmentDao;
import com.ems.daos.UserDao;
import com.ems.dtos.Response;
import com.ems.entities.Address;
import com.ems.entities.Department;
import com.ems.entities.JobHistory;
import com.ems.entities.User;
import com.ems.services.AddressService;
import com.ems.services.DepartmentService;
import com.ems.services.JobHistoryService;
import com.ems.services.UserService;
import com.ems.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(value = AdminController.class)
@AutoConfigureMockMvc(addFilters = false)
public class AdminControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private Response response;

	@MockBean
	private UserService service;

	@MockBean
	private UserDao dao;

	@MockBean
	private JwtUtil jwtUtil;

	@MockBean
	private DepartmentService deptService;

	@MockBean
	private DepartmentDao deptDao;

	@MockBean
	private AddressService addService;

	@MockBean
	private JobHistoryService jobService;

	private static SimpleDateFormat sdf;

	static {
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	@Test
	public void getAllEmployeeTest() throws Exception {

		List<User> userList = new ArrayList<>();
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		User u2 = new User(20, "saket", "sa@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		User u3 = new User(22, "Adesh", "ak@gmail.com", "1234", "1234456788", "Male", "User", 100000.00, null);
		userList.add(u1);
		userList.add(u2);
		userList.add(u3);

		Mockito.when(service.findAllUsers()).thenReturn(userList);
		String uri = "/admin/getAll";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

//		String expectedJson = this.mapToJson(userList);
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);

	}

	@Test
	public void addEmployeeTest() throws Exception {
		User user = new User(0, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00,
				new Department(1, "DevOps"));

		ObjectMapper mapper = new ObjectMapper();

		Mockito.when(service.saveUser(Mockito.any())).thenReturn(user);
		Mockito.when(dao.save(user)).thenReturn(user);

		String uri = "/admin/add";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(user)).characterEncoding("utf-8");

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);

	}

	@Test
	public void getEmployeeDetailsTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		Mockito.when(service.findByUserId(21)).thenReturn(u1);
		Mockito.when(dao.findUserById(21)).thenReturn(u1);
		String uri = "/admin/getEmployee/21";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void deleteEmployeeTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		Mockito.when(service.removeUser(21)).thenReturn(Collections.singletonMap("affected rows", 1));
//		Mockito.verify(dao,times(1)).deleteById(21);

		String uri = "/admin/deleteEmployeeById/21";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);

	}

	@Test
	public void getDepartmentDetails() throws Exception {

		Department d1 = new Department(1, "DevOps");
		Mockito.when(deptService.getDepartmentById(1)).thenReturn(d1);

		String uri = "/admin/department/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void allDepartmentTest() throws Exception {
		List<Department> departments = new ArrayList<Department>();
		Department d1 = new Department(1, "DevOps");
		Department d2 = new Department(2, "SPDE");
		Department d3 = new Department(3, "BDE");
		Department d4 = new Department(4, "Mobility");
		departments.add(d1);
		departments.add(d2);
		departments.add(d3);
		departments.add(d4);

		Mockito.when(deptService.getAllDepartments()).thenReturn(departments);
		Mockito.when(deptDao.findAll()).thenReturn(departments);

		String uri = "/admin/allDepartments";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void addDepartment() throws Exception {
		Department d1 = new Department(1, "DevOps");
		Mockito.when(deptService.addDepartment(Mockito.any()))
				.thenReturn(Collections.singletonMap("departmentAdded", 1));
		Mockito.when(deptDao.save(d1)).thenReturn(d1);
		ObjectMapper mapper = new ObjectMapper();

		String uri = "/admin/addDepartment";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(d1)).characterEncoding("utf-8");

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		assertNotNull(result.getResponse().getContentAsString());
		String output = result.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void getAllAddressesTest() throws Exception {
		List<Address> addressList = new ArrayList<Address>();
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
		Address a2 = new Address(2, "Trends", "Hyderabad", "India", "500032", u1);
		Address a3 = new Address(3, "Trends", "Hyderabad", "India", "500032", u1);
		addressList.add(a1);
		addressList.add(a2);
		addressList.add(a3);

		Mockito.when(addService.findAllAdd()).thenReturn(addressList);
		String uri = "/admin/allAddress";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = result.getResponse().getContentAsString();
		System.out.println(output);

	}

	@Test
	public void getAddressDetailsTest() throws Exception {
		List<Address> addressList = new ArrayList<Address>();
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);
		Address a1 = new Address(1, "Trends", "Hyderabad", "India", "500032", u1);
		Address a2 = new Address(2, "Trends", "Hyderabad", "India", "500032", u1);
		Address a3 = new Address(3, "Trends", "Hyderabad", "India", "500032", u1);
		addressList.add(a1);
		addressList.add(a2);
		addressList.add(a3);
		Mockito.when(addService.findByUserID(21)).thenReturn(Collections.singletonMap("Address ", addressList));
		String uri = "/admin/address/21";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = result.getResponse().getContentAsString();
		System.out.println(output);

	}

	@Test
	public void getAllJobTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		List<JobHistory> jobList = new ArrayList<JobHistory>();
		JobHistory j1 = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("30/08/2022"), u1);
		jobList.add(j1);

		Mockito.when(jobService.findAllJobs()).thenReturn(jobList);
		String uri = "/admin/allJob";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = result.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void getJobDetailsTest() throws Exception {
		User u1 = new User(21, "sanket", "sb@gmail.com", "1234", "1234567890", "Male", "User", 50.00, null);

		JobHistory j1 = new JobHistory(1, sdf.parse("26/05/2022"), sdf.parse("30/08/2022"), u1);

		Mockito.when(jobService.findByUserId(21)).thenReturn(j1);
		String uri = "/admin/job/21";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		
		String output = result.getResponse().getContentAsString();
		System.out.println(output);
		
	}

	

}
