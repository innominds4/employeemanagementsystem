package com.ems.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.ems.daos.AddressDao;
import com.ems.entities.Address;
import com.ems.entities.User;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AddressServiceTest {

	@Mock
	private AddressDao dao;
	
	@InjectMocks
	private AddressService service;
	
	@Test
	public void findByAddressIdTest() {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		Address address = new Address(1, "Trends", "Wipro Circle", "India", "500032", user);
		
		when(dao.findById(1)).thenReturn(address);
		
		assertEquals("Trends", service.findByAddId(1).getLandmark());
	}
	
	@Test
	public void findByUserIDTest() {
		User user = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		List<Address> addList = new ArrayList<Address>();
		addList.add(new Address(1, "Trends", "Wipro Circle", "India", "500032", user));
		when(dao.findByUserId(21)).thenReturn(addList);
		assertEquals(1, service.findByUserID(21).size());
	}
	
	@Test
	public void addAddressTest() {
		Address a1=(new Address(1, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		when(dao.save(a1)).thenReturn(a1);
		assertEquals(Collections.singletonMap("Address saved",1 ), service.addAddress(a1));
	}
	@Test
	public void findByPincodeTest() {
		List<User> userList = new ArrayList<>();
		User u1 = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		User u2 = new User(20,"saket", "sa@gmail.com","1234","1234567890","Male","User", 50.00, null);
		User u3 = new User(22, "Adesh", "ak@gmail.com", "1234","1234456788", "Male","User",100000.00, null);
		userList.add(u1);
		userList.add(u2);
		userList.add(u3);
		
		List <Address> addList = new ArrayList<Address>();
		addList.add(new Address(1, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		addList.add(new Address(2, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		addList.add(new Address(3, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		when(dao.findUserByPincode("500032")).thenReturn(userList);
		assertEquals(3, service.findUserByAddressId("500032").size());
	}
	
	@Test
	public void findAllTest() {
		List <Address> addList = new ArrayList<Address>();
		addList.add(new Address(1, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		addList.add(new Address(2, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		addList.add(new Address(3, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		
		when(dao.findAll()).thenReturn(addList);
		assertEquals(3, service.findAllAdd().size());
	}
	
	@Test
	public void removeAddressTest() {
		Address add = new Address(1, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null));
		when(dao.existsById(1)).thenReturn(true);
		service.removeAddress(1);
		verify(dao,times(1)).deleteById(1);
		
		when(dao.existsById(2)).thenReturn(false);
		service.removeAddress(2);
		verify(dao,times(0)).deleteById(null);
	}
	
	@Test
	public void updateAddress() {
		Address a1=(new Address(1, "Trends", "Wipro Circle", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		Address a2=(new Address(1, "Jodhpurwala", "Hyderabad", "India", "500032", new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null)));
		when(dao.existsById(1)).thenReturn(true);
		when(dao.save(a2)).thenReturn(a2);
		assertEquals(Collections.singletonMap("Address updated...!!!", 1).get(a2), service.updateAddress(a2).get(a2));
	}
	
	
}
