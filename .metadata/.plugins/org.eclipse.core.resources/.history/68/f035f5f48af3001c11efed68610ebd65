package com.ems.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ems.daos.DepartmentDao;
import com.ems.daos.UserDao;
import com.ems.dtos.DtoConverter;
import com.ems.entities.Department;
import com.ems.entities.User;
import com.ems.services.DepartmentService;
import com.ems.services.UserService;
import com.ems.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(value = AdminController.class)
@AutoConfigureMockMvc(addFilters = false)
public class AdminControllerTest  {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DtoConverter converter;
//	@Autowired
//	private ObjectMapper mapper;
	@MockBean
	private UserService service;
	
//	@InjectMocks
//	private AdminController controller;
	@MockBean
	private UserDao dao;
	
	@MockBean
	private JwtUtil jwtUtil;
	
	@MockBean
	private DepartmentService deptService;
	
	@MockBean
	private DepartmentDao deptDao;
	
	//@MockBean
//	private AdminController adminController;
	
//	@Override
//	@Before
//	protected void setUp() {
//		// TODO Auto-generated method stub
//		super.setUp();
//	}
	
	@Test
	public void getAllEmployeeTest() throws Exception{
		
		List<User> userList = new ArrayList<>();
		User u1 = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		User u2 = new User(20,"saket", "sa@gmail.com","1234","1234567890","Male","User", 50.00, null);
		User u3 = new User(22, "Adesh", "ak@gmail.com", "1234","1234456788", "Male","User",100000.00, null);
		userList.add(u1);
		userList.add(u2);
		userList.add(u3);
		
		
		Mockito.when(service.findAllUsers()).thenReturn(userList);
		String uri = "/admin/getAll";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk())
				.andReturn()
				;
	
		String expectedJson = this.mapToJson(userList);
		String outputJson =  mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
		
	}
	
	@Test
	public void addEmployeeTest() throws Exception {
		User user = new User(0,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, new Department(1,"DevOps"));

		ObjectMapper mapper = new ObjectMapper();
		
		
		Mockito.when(service.saveUser(Mockito.any())).thenReturn(user);
		Mockito.when(dao.save(user)).thenReturn(user);
		
		String uri = "/admin/add";
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(user))
				.characterEncoding("utf-8");
		
		
		MvcResult mvcResult = mockMvc
				.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	
	}
	
	@Test
	public void getEmployeeDetailsTest() throws Exception {
		User u1 = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		
		Mockito.when(service.findEmployeeById(21)).thenReturn(u1);
		Mockito.when(dao.findUserById(21)).thenReturn(u1);
		String uri = "/admin/getEmployee/21";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult mvcResult = mockMvc
				.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}
	

	@Test
	public void deleteEmployeeTest() throws Exception {
		User u1 = new User(21,"sanket", "sb@gmail.com","1234","1234567890","Male","User", 50.00, null);
		
		Mockito.when(service.removeUser(21)).thenReturn(Collections.singletonMap("affected rows", 1));
//		Mockito.verify(dao,times(1)).deleteById(21);
		
		String uri = "/admin/deleteEmployeeById/21";
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.delete(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult mvcResult = mockMvc
				.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		String output= mvcResult.getResponse().getContentAsString();
		System.out.println(output);
		
	}
	
	
	@Test
	public void getDepartmentDetails() throws Exception{
		
		Department d1 = new Department(1, "DevOps");
		Mockito.when(deptService.getDepartmentById(1)).thenReturn(d1);
		
		String uri = "/admin/department/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult mvcResult = mockMvc
				.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}

	@Test
	public void allDepartmentTest() throws Exception {
		List<Department> departments = new ArrayList<Department>();
		Department d1 = new Department(1, "DevOps");
		Department d2 = new Department(2, "SPDE");
		Department d3 = new Department(3, "BDE");
		Department d4 = new Department(4, "Mobility");
		departments.add(d1);
		departments.add(d2);
		departments.add(d3);
		departments.add(d4);
		
		Mockito.when(deptService.getAllDepartments()).thenReturn(departments);
		Mockito.when(deptDao.findAll()).thenReturn(departments);
		
		String uri = "/admin/allDepartments";
		
		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
				.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE);
		
		MvcResult mvcResult   = mockMvc
				.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		String output = mvcResult.getResponse().getContentAsString();
		System.out.println(output);
	}
		
	public String mapToJson(Object object) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}
	
	
	
	

}
